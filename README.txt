This package contains a bunch of Java classes and methods that I often use in my
Java coding.

Type ant dist dist-src to compile this project.

# ------------------------------------------------------------------------------

com.bluemarsh.graphmaker.core.util.*

    Contains Fibonacci  heap. Fibonacci heap can  decrease the key of  an object
    (min priority queue) but it cannot remove  the max object. If the max object
    has to be removed then I need to keep track of nodes, know which node I want
    to remove  and then  remove it. If  I want  to be able  to remove  max nodes
    automatically, I need  to use a min/max priority queue  instead. Taken from:
    git clone https://code.google.com/p/graphmaker/

http://code.google.com/p/guava-libraries/

    If I need a MinMax PQ, I can use the one from the above link.

tests.*

    Contains various tests, mostly junit tests.
