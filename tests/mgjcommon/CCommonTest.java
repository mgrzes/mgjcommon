package tests.mgjcommon;

import mgjcommon.CCommon;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: Marek Grzes
 * Date: 06/06/13 3:52 PM
 */
public class CCommonTest {
	@Test
	public void testIncvariance() throws Exception {
		double[] num = { 0.486,	0.5,	0.56,	0.9998567,	0.9888,	0.111,	0.001 };

		double avg = num[0];
		double sumsqr = 0;
		double prevavg = avg;
		for ( int i = 1; i < num.length; i++ ) {
			avg = CCommon.incmean(avg, num[i], i);
			sumsqr = CCommon.incvariance(sumsqr, num[i], prevavg, avg);
			prevavg = avg;
		}
		assertTrue("incremental average check", avg == 0.5209509571428571);
		System.out.println("avg=" + avg);
		assertTrue("incremental stdev check", Math.sqrt(sumsqr/(num.length - 1.0)) == 0.3851033545899242);
		System.out.println("stdev=" + Math.sqrt(sumsqr/(num.length - 1.0)));
	}
}
