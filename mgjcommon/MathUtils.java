package mgjcommon;

/**
 * Created with IntelliJ IDEA.
 * User: Marek Grzes
 * Date: 10/06/13 2:43 PM
 */
public class MathUtils {

	/**
	 *  We assume that nums are probabilities in the log scale. If numbers in nums are not normalised probabilities,
	 *  the normalisation can be done using:
	 *              nums[i] = nums[i] - logsumexp(nums)
	 *  This would correspond to muls[i] = nums[i] / sum_i nums[i] when nums would be in non-log scale.
	 *
	 *  For more info, see http://www.cs.ubc.ca/~murphyk/Teaching/CS340-Fall06/reading/NB.pdf
	 *
	 *  @param nums is the list of probabilities in the log scale; this function computes the sum of these probabilities
	 *              and returns the summation in the log scale!
	 *  @return \log \sum_t e^{a_t} = \log \sum_t e^{a_t}e^{A-A} = A+ \log\sum_t e^{a_t -A}
	 */
	static public double logsumexp(double[] nums) {
		double max_exp = nums[0], sum = 0.0;

		for (int i = 1 ; i < nums.length ; i++) {
			if ( nums[i] > max_exp ) {
				max_exp = nums[i];
			}
		}

		for (int i = 0; i < nums.length ; i++) {
			sum += Math.exp(nums[i] - max_exp);
		}

		return Math.log(sum) + max_exp;
	}

	static public double logsumexp(double a, double b) {
		double[] tmp = new double[2];
		tmp[0] = a;
		tmp[1] = b;
		return logsumexp(tmp);
	}

	static public double logsumexp(double a, double b, double c) {
		double[] tmp = new double[3];
		tmp[0] = a;
		tmp[1] = b;
		tmp[2] = c;
		return logsumexp(tmp);
	}

	public static void main(String[] args) {
		// a list numbers that represent un-normalised probabilities in the log scale
		double[] d = { -1000, -1005, -1010 };

		// log exp sum
		double logsumexp = MathUtils.logsumexp(d);

		System.out.println("logsumexp = " + logsumexp);

		double origSum = 0;
		for ( double x : d ) {
			double normalised = x - logsumexp; // normalisation in the log scale is just substraction of the sum
			System.out.println(x + " after normalisation becomes " + (normalised) + " which makes " + Math.exp(normalised) );
			origSum += Math.exp(normalised);
		}
		System.out.println("original sum is " + origSum);
		// numbers after normalisation can be used to compute probs. in the non-log space and then sampling can happen
		// in the original space; very low logs will become probabilities close to zero and they won't be sampled which is fine
	}
}
