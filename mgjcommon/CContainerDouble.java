package mgjcommon;

/// This container is in order to return double values via parameters to methods.
public class CContainerDouble {
	public double _double;
	public CContainerDouble() {}
	public CContainerDouble(double d) {
		_double = d;
	}
	public boolean equals(Object b) {
		if ( _double == ( (CContainerDouble) b)._double) {
			return true;
		}
		return false;
	}
	
	public boolean lessthan(Object b) {
		return lessthan((CContainerDouble)b);
	}
	
	/// @return true when this is < b.
	public boolean lessthan(CContainerDouble b) {
		if ( _double < b._double) {
			return true;
		}
		return false;
	}
}
