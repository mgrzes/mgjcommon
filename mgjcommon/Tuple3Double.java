package mgjcommon;

public final class Tuple3Double {
    public double x1;
    public double x2;
	public double x3;

    public Tuple3Double() {
    	super();
    }

    public Tuple3Double(double first, double second, double third) {
        super();
        this.x1 = first;
        this.x2 = second;
		this.x3 = third;
    }

    public final int hashCode() {
        return (int)(x1 * 10 + x2 * 100 + x3 * 1000);
    }

    public final boolean equals(Object other) {
        if (other instanceof Tuple3Double) {
        		@SuppressWarnings("unchecked")
				Tuple3Double otherPair = (Tuple3Double) other;
                return 
                ( ( this.x1 == otherPair.x1 ) && (  this.x2 == otherPair.x2 ) && (  this.x3 == otherPair.x3 ) );
        }

        return false;
    }

    public String toString()  {
           return "<" + x1 + ", " + x2 + ", " + x3 + ">";
    }
}
