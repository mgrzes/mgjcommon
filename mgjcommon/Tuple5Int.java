package mgjcommon;

public final class Tuple5Int {
    public int x1;
    public int x2;
	public int x3;
	public int x4;
	public int x5;

    public Tuple5Int() {
    	super();
    }

    public Tuple5Int(int first, int second, int third, int fourth, int fifth) {
        super();
        this.x1 = first;
        this.x2 = second;
		this.x3 = third;
		this.x4 = fourth;
		this.x5 = fifth;
    }

    public final int hashCode() {
        return x1 + x2 + x3 + x4 + x5;
    }

    public final boolean equals(Object other) {
        if (other instanceof Tuple5Int) {
        		@SuppressWarnings("unchecked")
				Tuple5Int otherPair = (Tuple5Int) other;
                return 
                ( ( this.x1 == otherPair.x1 ) && (  this.x2 == otherPair.x2 ) && (  this.x3 == otherPair.x3 ) && (  this.x4 == otherPair.x4 ) && (  this.x5 == otherPair.x5 ) );
        }

        return false;
    }

    public String toString()  {
           return "<" + x1 + ", " + x2 + ", " + x3 + ", " + x4 + ", " + x5 + ">";
    }
}
