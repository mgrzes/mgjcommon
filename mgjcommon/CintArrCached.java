/**
 * @author Marek Grzes <mgrzes@uwaterloo.ca>
 *
 */
package mgjcommon;

/**
 * This array can have some large capacity that is not reset when
 * the clear method is invoked.
 *
 */
public final class CintArrCached {
	public int[] arr;
	/**
	 * This is the actual number of elements stored in the array.
	 */
	public int size;
	protected int _capacity;
	protected int _increment;

	public CintArrCached() {
		this(100, 100);
	}
	
	public CintArrCached(int initialCapacity, int increment) {
		if ( initialCapacity < 0 ) {
			throw new IllegalArgumentException( "Illegal Capacity: " + initialCapacity );
		}
		arr = new int[initialCapacity];
		size = 0;
		_capacity = initialCapacity;
		_increment = increment;
	}
	
	public final void add(int element) {
		size++;
		if ( _capacity >= size ) {
			arr[size - 1] = element;
		} else {
			_capacity += _increment;
			int[] newArray = new int[_capacity];
			System.arraycopy(arr,0,newArray,0,arr.length);
			arr = newArray;
			arr[size - 1] = element;
		}
	}

	/**
	 * <b>Data is not copied if the array is resized.</b> The method
	 * just makes sure that we have newSize objects of type int in the array.
	 */
	public final void resize(int newSize) {
		size=newSize;
		if ( _capacity < size ) {
			_capacity = size;
			arr = new int[_capacity];
		}
	}
	
	/**
	 * The container will keep the data, only the size is set to 0.
	 */
	public final void clear() {
		size = 0;
	}
	
	public final int size() {
		return size;
	}
}
