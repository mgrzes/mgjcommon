package mgjcommon;

public final class Tuple2DoubleObj<C> {
    public double x1;
    public double x2;
	public C x3;

    public Tuple2DoubleObj() {
    	super();
    }

    public Tuple2DoubleObj(double first, double second, C third) {
        super();
        this.x1 = first;
        this.x2 = second;
		this.x3 = third;
    }

    public final int hashCode() {
        return (int)(x1 * 10 + x2 * 100 + x3.hashCode() * 1000);
    }

    public final boolean equals(Object other) {
        if (other instanceof Tuple2DoubleObj) {
        		@SuppressWarnings("unchecked")
                Tuple2DoubleObj otherPair = (Tuple2DoubleObj) other;
                return 
                ( ( this.x1 == otherPair.x1 ) && (  this.x2 == otherPair.x2 ) && (  this.x3.equals(otherPair.x3) ) );
        }

        return false;
    }

    public String toString()  {
           return "<" + x1 + ", " + x2 + ", " + x3.toString() + ">";
    }
}
