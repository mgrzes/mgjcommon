package mgjcommon;

public final class CircularBufferArray<B> {
    boolean full = false;
	// index points to the last valid item that was added in the previous iteration
    int index = -1;
    public B[] data;
	int iterIndex;

    public CircularBufferArray(int size) {
        super();
        this.data = (B[]) new Object[size];
    }

    public final int hashCode() {
        return data.hashCode();
    }

    public final void push(B item) {
        index++;
        if ( full = false && index == data.length) {
            full = true;
        }
        index = index % data.length;
        data[index] = item;
    }

    public String toString() {
           return data.toString();
    }

	public void reset() {
		iterIndex = -1;
	}

	public boolean hasNext() {
		if ( full || iterIndex + 1 <= index ) {
			return true;
		} else {
			return false;
		}
	}

	public B next() {
		iterIndex++;
		return data[iterIndex];
	}
}
