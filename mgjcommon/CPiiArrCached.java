/**
 * @author Marek Grzes <mgrzes@uwaterloo.ca>
 *
 */
package mgjcommon;

/**
 * Array of PairIntInt objects.
 *  
 * This array can have some large capacity that is not reset when
 * the clear method is invoked. The main point of this method
 * is that it caches not only pointers to PairIntInt but also
 * PairIntInt objects in themselves. This makes sense because
 * PairIntInt is a mutable object. It would make sense to do
 * the same with Integer or Double.
 */
public final class CPiiArrCached {
	public PairIntInt[] arr;
	/**
	 * This is the actual number of elements stored in the array.
	 */
	public int size;
	protected int _capacity;
	protected int _increment;

	public CPiiArrCached() {
		this(100, 100);
	}
	
	public CPiiArrCached(int initialCapacity, int increment) {
		if ( initialCapacity < 0 ) {
			throw new IllegalArgumentException( "Illegal Capacity: " + initialCapacity );
		}
		arr = new PairIntInt[initialCapacity];
		for ( int i = 0; i < initialCapacity; i++ ) {
			arr[i] = new PairIntInt();
		}
		size = 0;
		_capacity = initialCapacity;
		_increment = increment;
	}
	
	/**
	 * <b>Memory is reused when this method is called!</b>
	 * @param f
	 * @param s
	 */
	public final void add(int f, int s) {
		size++;
		if ( _capacity >= size ) {
			arr[size - 1].first = f;
			arr[size - 1].second = s;
		} else {
			_capacity += _increment;
			PairIntInt[] newArray = new PairIntInt[_capacity];
			System.arraycopy(arr,0,newArray,0,arr.length);
			for ( int i = arr.length; i < _capacity; i++ ) {
				newArray[i] = new PairIntInt();
			}
			arr = newArray;
			arr[size - 1].first = f;
			arr[size - 1].second = s;
		}
	}

	/**
	 * <b>Data is not copied if the array is resized.</b> The method
	 * just makes sure that we have newSize objects of type int in the array.
	 */
	public final void resize(int newSize) {
		size=newSize;
		if ( _capacity < size ) {
			arr = new PairIntInt[newSize];
			for ( int i = _capacity; i < newSize; i++ ) {
				arr[i] = new PairIntInt();
			}
		}
	}
	
	/**
	 * The container will keep the data, only the size is set to 0.
	 */
	public final void clear() {
		size = 0;
	}
	
	public final int size() {
		return size;
	}
}
