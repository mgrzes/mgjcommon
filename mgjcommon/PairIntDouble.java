package mgjcommon;

/**
 * Created with IntelliJ IDEA.
 * User: Marek Grzes
 * Date: 09/11/12 9:57 AM
 */

public class PairIntDouble {
	public int first;
	public double second;

	public PairIntDouble() {
		super();
	}

	public PairIntDouble(int first, double second) {
		super();
		this.first = first;
		this.second = second;
	}

	public final int hashCode() {
		int hashSecond = (int)second;
		return (first + hashSecond) * hashSecond + first;
	}

	public final boolean equals(Object other) {
		if (other instanceof PairIntDouble) {
			@SuppressWarnings("unchecked")
			PairIntDouble otherPair = (PairIntDouble) other;
			return
					( ( this.first == otherPair.first ) && (  this.second == otherPair.second) );
		}

		return false;
	}

	public String toString()
	{
		return "(" + first + ", " + second + ")";
	}
}
