package mgjcommon;

/**
 * Created with IntelliJ IDEA.
 * User: Marek Grzes
 * Date: 28/03/13 1:27 PM
 */

import gnu.trove.iterator.TObjectLongIterator;
import gnu.trove.map.TObjectLongMap;
import gnu.trove.map.hash.TObjectLongHashMap;

public class Profiler {
	static private TObjectLongMap _map = new TObjectLongHashMap<String>();
	static private long _start;

	/** removes all elements from the map */
	static public void clear() {
		_map.clear();
	}

	/** defines new point */
	static public void definePoint(String name) {
		_map.put(name, 0);
	}

	/** increases the time of the point */
	static private void addValue(String name, long diff) {
		_map.adjustValue(name, diff);
	}

	static public void print() {
		TObjectLongIterator<String> iter = _map.iterator();
		while( iter.hasNext()) {
			iter.advance();
			System.out.println(iter.key() + "\t" + iter.value()/1000.0 + " seconds, " + iter.value() + " milli-seconds");
		}
	}

	/** values of all points are reset to 0 */
	static public void reset() {
		TObjectLongIterator<String> iter = _map.iterator();
		while( iter.hasNext()) {
			iter.advance();
			iter.setValue(0);
		}
	}

	/** starts the timer again but does not reset current data, hence it is possible to start the timer many times */
	static public long tstart() {
		_start = System.currentTimeMillis();
		return _start;
	}

	static public void recordPoint(String name) {
		long tmp = System.currentTimeMillis();
		addValue(name, tmp - _start);
		_start = tmp;
	}

	public static void main(String[] args) throws Exception {
		Profiler.definePoint("line1");
		Profiler.tstart();

		Thread.sleep(2000);

		Profiler.recordPoint("line1");

		Thread.sleep(2000);

		Profiler.recordPoint("line1");

		Profiler.print();

	}

}
