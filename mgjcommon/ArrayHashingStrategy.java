package mgjcommon;

import gnu.trove.strategy.HashingStrategy;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: Marek Grzes
 * Date: 25/09/12
 * Time: 2:19 PM
 */

// The set can be created as follows:
// Set<String[]> set = new TCustomHashSet<String[]>(new ArrayHashingStrategy());

// TODO: I had problems with this class and it did not work properly!
class ArrayHashingStrategy implements HashingStrategy<Object[]> {

	public int computeHashCode(Object[] array) {
		return Arrays.hashCode(array);
	}

	public boolean equals(Object[] arr1, Object[] arr2) {
		return Arrays.equals(arr1, arr2);
	}
}
