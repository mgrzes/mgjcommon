package mgjcommon;

public final class Tuple4Int {
    public int x1;
    public int x2;
	public int x3;
	public int x4;

    public Tuple4Int() {
    	super();
    }

    public Tuple4Int(int first, int second, int third, int fourth) {
        super();
        this.x1 = first;
        this.x2 = second;
		this.x3 = third;
		this.x4 = fourth;
    }

    public final int hashCode() {
        return x1 + x2 + x3 + x4;
    }

    public final boolean equals(Object other) {
        if (other instanceof Tuple4Int) {
        		@SuppressWarnings("unchecked")
				Tuple4Int otherPair = (Tuple4Int) other;
                return 
                ( ( this.x1 == otherPair.x1 ) && (  this.x2 == otherPair.x2 ) && (  this.x3 == otherPair.x3 ) && (  this.x4 == otherPair.x4 ) );
        }

        return false;
    }

    public String toString()  {
           return "<" + x1 + ", " + x2 + ", " + x3 + ", " + x4 + ">";
    }
}
