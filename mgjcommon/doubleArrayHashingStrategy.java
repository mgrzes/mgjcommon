package mgjcommon;

import gnu.trove.strategy.HashingStrategy;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: Marek Grzes
 * Date: 25/09/12
 * Time: 2:17 PM
 */

// The set should be created as follows:
// Set<double[]> res = new TCustomHashSet<double[]>(new doubleArrayHashingStrategy());

// TODO: I had problems with this class and it did not work properly!
public class doubleArrayHashingStrategy implements HashingStrategy<double[]> {

	public int computeHashCode(double[] array) {
		return Arrays.hashCode(array);
	}

	public boolean equals(double[] arr1, double[] arr2) {
		return Arrays.equals(arr1, arr2);
	}

}
