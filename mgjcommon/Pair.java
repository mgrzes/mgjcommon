/**
 * This is a useful equivalent of stl::pair from C++ that I found somewhere
 * in the Internet.
 */

package mgjcommon;

public final class Pair<A, B> {
    public A first;
    public B second;

    public Pair(A first, B second) {
        super();
        this.first = first;
        this.second = second;
    }

    public final int hashCode() {
        int hashFirst = first != null ? first.hashCode() : 0;
        int hashSecond = second != null ? second.hashCode() : 0;

        return (hashFirst + hashSecond) * hashSecond + hashFirst;
    }

    public final boolean equals(Object other) {
        if (other instanceof Pair) {
        		@SuppressWarnings("unchecked")
                Pair<A,B> otherPair = (Pair<A,B>) other;
                return 
                ((  this.first == otherPair.first ||
                        ( this.first != null && otherPair.first != null &&
                          this.first.equals(otherPair.first))) &&
                 (      this.second == otherPair.second ||
                        ( this.second != null && otherPair.second != null &&
                          this.second.equals(otherPair.second))) );
        }

        return false;
    }

    public String toString()
    { 
           return "(" + first + ", " + second + ")"; 
    }

    public final A first() {
        return first;
    }
   
    public final void first(A first) {
        this.first = first;
    }
    
    public final B second() {
        return second;
    }
    
    public final void second(B second) {
        this.second = second;
    }
}

