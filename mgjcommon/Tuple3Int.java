package mgjcommon;

public final class Tuple3Int {
    public int x1;
    public int x2;
	public int x3;

    public Tuple3Int() {
    	super();
    }

    public Tuple3Int(int first, int second, int third) {
        super();
        this.x1 = first;
        this.x2 = second;
		this.x3 = third;
    }

	public Tuple3Int(Tuple3Int other) {
		super();
		this.x1 = other.x1;
		this.x2 = other.x2;
		this.x3 = other.x3;
	}

    public final int hashCode() {
        return x1 + x2 + x3;
    }

    public final boolean equals(Object other) {
        if (other instanceof Tuple3Int) {
        		@SuppressWarnings("unchecked")
				Tuple3Int otherPair = (Tuple3Int) other;
                return 
                ( ( this.x1 == otherPair.x1 ) && (  this.x2 == otherPair.x2 ) && (  this.x3 == otherPair.x3 ) );
        }

        return false;
    }

    public String toString()  {
           return "<" + x1 + ", " + x2 + ", " + x3 + ">";
    }
}
