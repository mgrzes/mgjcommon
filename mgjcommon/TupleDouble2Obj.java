package mgjcommon;

public final class TupleDouble2Obj<B,C> {
    public double x1;
    public B x2;
	public C x3;

    public TupleDouble2Obj() {
    	super();
    }

    public TupleDouble2Obj(double first, B second, C third) {
        super();
        this.x1 = first;
        this.x2 = second;
		this.x3 = third;
    }

    public final int hashCode() {
        return (int)(x1 * 10 + x2.hashCode() * 100 + x3.hashCode() * 1000);
    }

    public final boolean equals(Object other) {
        if (other instanceof TupleDouble2Obj) {
        		@SuppressWarnings("unchecked")
				TupleDouble2Obj otherPair = (TupleDouble2Obj) other;
                return 
                ( ( this.x1 == otherPair.x1 ) && (  this.x2.equals(otherPair.x2) ) && (  this.x3.equals(otherPair.x3) ) );
        }

        return false;
    }

    public String toString()  {
           return "<" + x1 + ", " + x2.toString() + ", " + x3.toString() + ">";
    }
}
