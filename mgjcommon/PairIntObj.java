/**
 * This is a useful equivalent of stl::pair from C++.
 * This one if for the case when the first element is integer.
 */

package mgjcommon;

public final class PairIntObj<B> {
    public int first;
    public B second;

    public PairIntObj(int first, B second) {
        super();
        this.first = first;
        this.second = second;
    }

    public final int hashCode() {
        int hashSecond = second != null ? second.hashCode() : 0;

        return (first + hashSecond) * hashSecond + first;
    }

    public final boolean equals(Object other) {
        if (other instanceof PairIntObj) {
        		@SuppressWarnings("unchecked")
                PairIntObj<B> otherPair = (PairIntObj<B>) other;
                return 
                ((  this.first == otherPair.first ) &&
                 (  this.second == otherPair.second ||
                        ( this.second != null && otherPair.second != null &&
                          this.second.equals(otherPair.second))) );
        }

        return false;
    }

    public String toString()
    { 
           return "(" + first + ", " + second + ")"; 
    }
}
