package mgjcommon;

/**
 * Created with IntelliJ IDEA.
 * User: Marek Grzes
 * Date: 05/09/13 10:46 PM
 */
public class HTMLColours {

	public int _numColours = 100;
	public String[] _colours;

	public HTMLColours() {
		_colours = new String[_numColours];
		_colours[0] = new String("\"#FF0000\"");
		_colours[1] = new String("\"#00FF00\"");
		_colours[2] = new String("\"#0000FF\"");
		_colours[4] = new String("\"#000000\"");
		_colours[5] = new String("\"#FFFF00\"");
		_colours[6] = new String("\"#FF00FF\"");
		_colours[7] = new String("\"#00FFFF\"");
		_colours[8] = new String("\"#8A2BE2\"");
		_colours[9] = new String("\"#7FFF00\"");
		_colours[10] = new String("\"#B8860B\"");
		_colours[11] = new String("\"#00CED1\"");
		_colours[12] = new String("\"#1E90FF\"");
		_colours[13] = new String("\"#008000\"");
		_colours[14] = new String("\"#9370DB\"");
		_colours[15] = new String("\"#000F0F\"");
		_colours[16] = new String("\"#00F00F\"");
		_colours[17] = new String("\"#0F000F\"");
		_colours[18] = new String("\"#F0000F\"");
		_colours[19] = new String("\"#000FFF\"");
		_colours[20] = new String("\"#00F0FF\"");
		_colours[21] = new String("\"#000007\"");
		_colours[22] = new String("\"#007000\"");
		_colours[23] = new String("\"#070000\"");
		_colours[24] = new String("\"#700000\"");
		_colours[25] = new String("\"#000707\"");
		_colours[26] = new String("\"#007007\"");
		_colours[27] = new String("\"#070007\"");
		_colours[28] = new String("\"#700007\"");
		_colours[29] = new String("\"#000777\"");
		_colours[30] = new String("\"#007077\"");
		_colours[31] = new String("\"#000007\"");
		_colours[32] = new String("\"#007000\"");
		_colours[33] = new String("\"#070000\"");
		_colours[34] = new String("\"#700000\"");
		_colours[35] = new String("\"#000707\"");
		_colours[36] = new String("\"#007007\"");
		_colours[37] = new String("\"#070007\"");
		_colours[38] = new String("\"#F0F8FF\"");
		_colours[39] = new String("\"#FF6347\"");
		_colours[40] = new String("\"#9ACD32\"");

		// all the rest is black for now
		for ( int i = 41; i < _numColours; i++ ) {
			_colours[i] = new String("\"#000000\"");
		}

	}

}
