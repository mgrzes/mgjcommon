/**
 * @author      Marek Grzes	<mgrzes@uwaterloo.ca>
 * @version                        
 * @since       2012-02-10
 */

package mgjcommon;

public final class CRuntimeConfig {
	public final static boolean _DEBUG = false;
	public final static boolean _PERFORMANCE_TEST = false;
	
	/**
	 *  This may print a lot of logs form the code that is not executed very often.
	 */
	public final static boolean _VERBOSE = false;
	
	/**
	 * Logs from the code which is executed often.
	 */
	public final static boolean _VERBOSE2 = false;
	public final static boolean _VERBOSE3 = false;
}
