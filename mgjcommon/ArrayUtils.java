package mgjcommon;

/**
 * Created with IntelliJ IDEA.
 * User: Marek Grzes
 * Date: 08/06/14 12:19
 */
public class ArrayUtils {

	/**
	 * @param ar1
	 * @param ar2
	 * @param precision
	 * @return true when ar1 == ar2 element-wise up to a given precision
	 */
	static public boolean equal(double[] ar1, double[] ar2, double precision) {
		if ( ar1.length != ar2.length ) return false;

		for ( int i = 0; i < ar1.length; i++ ) {
			if ( Math.abs(ar1[i] - ar2[i]) > precision ) return false;
		}

		return true;
	}

	static public boolean equal(double[] ar1, double[] ar2) {
		if ( ar1.length != ar2.length ) return false;

		for ( int i = 0; i < ar1.length; i++ ) {
			if ( ar1[i] != ar2[i] ) return false;
		}

		return true;
	}

}
