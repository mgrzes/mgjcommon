/**
 * @author      Marek Grzes	<mgrzes@uwaterloo.ca>
 * @version                        
 * @since       2012-02-27
 */

package mgjcommon;


import java.io.File;
import java.util.Arrays;

public class CCommon {
	/**
	 * Prints info about java version etc.
	 */
	public static void sys_info() {
		System.out.println("-----");
		System.out.println("java.version is \""+System.getProperty("java.version")+"\"");
		System.out.println("java.vendor is \""+System.getProperty("java.vendor")+"\"");
		System.out.println("java.vendor.url is \""+System.getProperty("java.vendor.url")+"\"");
		System.out.println("java.class.version Java API version is \""+System.getProperty("java.class.version")+"\"");
		System.out.println("java.class.path CLASSPATH is \""+System.getProperty("java.class.path")+"\"");
		System.out.println("LD_LIBRARY_PATH is \""+System.getProperty("java.library.path")+"\"");
		System.out.println("-----");
	}
	
	public static String getCurrentMethodName() {
		StackTraceElement stackTraceElements[] = (new Throwable()).getStackTrace();
		return stackTraceElements[1].toString();
	}

	public static int argmax(double[] ar) {
		int bestIdx = -1;
		double max = Double.NEGATIVE_INFINITY;
		for (int i = 0; i < ar.length; i++) {
			double elem = ar[i];
			if (elem > max) {
				max = elem;
				bestIdx = i;
			}
		}
		return bestIdx;
	}

	public static int argmin(double[] ar) {
		int bestIdx = -1;
		double min = Double.POSITIVE_INFINITY;
		for (int i = 0; i < ar.length; i++) {
			double elem = ar[i];
			if (elem < min) {
				min = elem;
				bestIdx = i;
			}
		}
		return bestIdx;
	}

	public static double findMin(double[][] ar) {
		double minValue = Double.POSITIVE_INFINITY;
		for ( double[] d1 : ar ) {
			for ( double d : d1 ) {
				if ( minValue > d ) {
					minValue = d;
				}
			}
		}
		return minValue;
	}

	public static double findMax(double[][] ar) {
		double maxValue = Double.NEGATIVE_INFINITY;
		for ( double[] d1 : ar ) {
			for ( double d : d1 ) {
				if ( maxValue < d ) {
					maxValue = d;
				}
			}
		}
		return maxValue;
	}

	/**
	 * @param arr
	 * @param precision
	 * @return true if there is at most one non-zero up to a given precision
	 */
	public static boolean atMostOneNonZero(double[] arr, double precision) {
		int n = 0;
		for ( double x: arr) {
			if ( Math.abs(x) > precision ) {
				n++;
			}
			if ( n > 1) return false;
		}

		return true;
	}

	/**
	 * Found at: http://stackoverflow.com/questions/924394/how-to-get-file-name-without-the-extension 
	 */
    public static String stripExtension (String str) {
        if (str == null) return null;

        // Get position of last '.'.
        int pos = str.lastIndexOf(".");

        // If there wasn't any '.' just return the string as is.
        if (pos == -1) return str;

        // Otherwise return the string, up to the dot.
        return str.substring(0, pos);
    }

	public String filename(String fullPath) { // gets filename without extension
		int dot = fullPath.lastIndexOf( "." );
		int sep = fullPath.lastIndexOf( File.pathSeparator );
		return fullPath.substring(sep + 1, dot);
	}

	public String extension(String fullPath) {
		int dot = fullPath.lastIndexOf( "." );
		return fullPath.substring(dot + 1);
	}

	public String path(String fullPath) {
		int sep = fullPath.lastIndexOf( File.pathSeparator );
		return fullPath.substring(0, sep);
	}

	/**
	 * Found here: http://stackoverflow.com/questions/202302/rounding-to-an-arbitrary-number-of-significant-digits
	 * @param num
	 * @param n
	 * @return
	 */
	public static double roundToSignificantFigures(double num, int n) {
		if(num == 0) {
			return 0;
		}

		final double d = Math.ceil(Math.log10(num < 0 ? -num: num));
		// without this check we cannot round 0.00000000001234 to 5 digits correctly
		if ( -d > n ) {
			return 0.0;
		}
		final int power = n - (int) d;

		final double magnitude = Math.pow(10, power);
		final long shifted = Math.round(num*magnitude);
		return shifted/magnitude;
	}

	/**
	 * Found here: http://stackoverflow.com/questions/202302/rounding-to-an-arbitrary-number-of-significant-digits
	 * The maximum double value in Java is on the order of 10^308, while the minimum value is on the order of 10^-324.
	 * Therefore, you can run into trouble when applying the function roundToSignificantFigures to something
	 * that's within a few powers of ten of Double.MIN_VALUE. For example, when you call
	 * roundToSignificantFigures(1.234E-310, 3);
	 * then the variable power will have the value 3 - (-309) = 312. Consequently, the variable magnitude
	 * will become Infinity, and it's all garbage from then on out. Fortunately, this is not an insurmountable
	 * problem: it is only the factor magnitude that's overflowing. What really matters is the product num * magnitude,
	 * and that does not overflow. One way of resolving this is by breaking up the multiplication
	 * by the factor magnitude into two steps:
	 */
	public static double roundToNumberOfSignificantDigits(double num, int n) {

		final double maxPowerOfTen = Math.floor(Math.log10(Double.MAX_VALUE));

		if(num == 0) {
			return 0;
		}

		final double d = Math.ceil(Math.log10(num < 0 ? -num: num));
		final int power = n - (int) d;

		double firstMagnitudeFactor = 1.0;
		double secondMagnitudeFactor = 1.0;
		if (power > maxPowerOfTen) {
			firstMagnitudeFactor = Math.pow(10.0, maxPowerOfTen);
			secondMagnitudeFactor = Math.pow(10.0, (double) power - maxPowerOfTen);
		} else {
			firstMagnitudeFactor = Math.pow(10.0, (double) power);
		}

		double toBeRounded = num * firstMagnitudeFactor;
		toBeRounded *= secondMagnitudeFactor;

		final long shifted = Math.round(toBeRounded);
		double rounded = ((double) shifted) / firstMagnitudeFactor;
		rounded /= secondMagnitudeFactor;
		return rounded;
	}

	public static double pruneToSignificantFigures(double num, int n) {
		if(num == 0) {
			return 0;
		}

		final double d = Math.ceil(Math.log10(num < 0 ? -num: num));
		// without this check we cannot prune 0.00000000001234 to 5 digits correctly
		if ( -d > n ) {
			return 0.0;
		}
		final int power = n - (int) d;

		final double magnitude = Math.pow(10, power);
		if ( num > 0 ) {
			final double shifted = Math.floor(num*magnitude);
			return shifted/magnitude;
		} else {
			final double shifted = Math.ceil(num*magnitude);
			return shifted/magnitude;
		}
	}

	/**
	 * Computes magnitude. For example, magnitude(173345,3) should return 1000.
	 *
	 * @see CCommon#pruneToSignificantFigures
	 * @param num
	 * @param n
	 * @return magnitude after pruning to significant digits (<b>negative when num is negative!</b>)
	 */
	public static double magnitude(double num, int n) {
		if(num == 0) {
			return 0;
		}

		final double d = Math.ceil(Math.log10(num < 0 ? -num: num));
		// without this check we cannot prune 0.00000000001234 to 5 digits correctly
		if ( -d > n ) {
			return 0.0;
		}
		final int power = (int) d - n;

		final double magnitude = Math.pow(10, power);
		if ( num > 0 )
			return magnitude;
		else
			return -magnitude;
	}

	/**
	 * A_{n+1} = A_{n} + (v_{n+1} - A_{n}) / (n + 1)
	 *
	 * An incremental procedure for computing a mean value. The first value becomes prevMean, and the function should
	 * be called from the second value when (nextIndex=1). First value's index is 0.
	 * @param prevMean
	 * @param nextValue
	 * @param nextIndex the number of the next value
	 * @return the next mean
	 */
	public static double incmean(double prevMean, double nextValue, int nextIndex) throws Exception {
		if ( CRuntimeConfig._DEBUG ) {
			if ( nextIndex < 1 ) throw new Exception("nextIndex has to be greater than one; " + nextIndex);
		}
		return prevMean + ( nextValue - prevMean ) / ((double)nextIndex + 1.0);
	}

	/**
	 * S_{n} = S_{n−1} + (x_{n} − avg_{n−1})(x_{n} − avg_{n})
	 * http://nfs-uxsup.csx.cam.ac.uk/~fanf2/hermes/doc/antiforgery/stats.pdf
	 *
	 * Procedure used for computing incremental variance. In fact, computes an incremental sum of squares. In order to
	 * compute variance the sum of squares has to be divided by N.
	 * @param prevSumSquares
	 * @param nextValue
	 * @param prevMean
	 * @param currMean
	 * @return current sum of squares
	 * @throws Exception
	 */
	public static double incvariance(double prevSumSquares, double nextValue, double prevMean, double currMean) throws Exception {
		return prevSumSquares + (nextValue - prevMean) * ( nextValue - currMean);
	}

	/**
	 * Having cumulative Dirichlet distribution, get max index of the outcome whose cdf[index]<=r. Sampling in time
	 * O(log(n)) + plus some linear time adjustments in order to cope with zero probabilities.
	 * @param cdf
	 * @param r
	 * @param start - first valid index
	 * @param end - last valid index
	 * @return
	 */
	public static int searchSample(double[] cdf, double r, int start, int end) throws Exception {
		if ( start == end ) {
			if ( cdf[start] == 0.0 ) {
				while ( cdf[start] == 0.0 ) {
					// go forward to the first non-zero probability index
					start++;
					if ( start >= cdf.length ) {
						throw new Exception("wrong multinomial CDF: " + Arrays.toString(cdf) + ", r = " + r);
					}
				}
			} else if ( start > 0 && cdf[start] == cdf[start-1]) {
				while ( start > 0 && cdf[start] == cdf[start-1]) {
					// go back to the non-zero probability index
					start--;
				}
				if ( cdf[start] == 0.0 ) {
					throw new Exception("wrong multinomial CDF: " + Arrays.toString(cdf) + ", r = " + r);
				}
			}
			return start;
		}
		int median = (end - start) / 2 + start;
		if ( cdf[median] >= r ) {
			return searchSample(cdf, r, start, median);
		} else {
			if ( median == start ) return searchSample(cdf, r, median + 1, end);
			return searchSample(cdf, r, median, end);
		}
	}

	public static int searchSample2(double[] cdf, double r, int start, int end) throws Exception {
		if ( r == 0.0 ) {
			// return the first non-zero probability event

		}
		if ( r >= cdf[end] ) {
			// return the last non-zero probability event
		}
		if ( start == end ) {
			if ( cdf[start] == 0.0 ) {
				while ( cdf[start] == 0.0 ) {
					// go forward to the first non-zero probability index
					start++;
					if ( start >= cdf.length ) {
						throw new Exception("wrong multinomial CDF: " + Arrays.toString(cdf) + ", r = " + r);
					}
				}
			} else if ( start > 0 && cdf[start] == cdf[start-1]) {
				while ( start > 0 && cdf[start] == cdf[start-1]) {
					// go back to the non-zero probability index
					start--;
				}
				if ( cdf[start] == 0.0 ) {
					throw new Exception("wrong multinomial CDF: " + Arrays.toString(cdf) + ", r = " + r);
				}
			}
			return start;
		}
		int median = (end - start) / 2 + start;
		if ( cdf[median] >= r ) {
			return searchSample(cdf, r, start, median);
		} else {
			if ( median == start ) return searchSample(cdf, r, median + 1, end);
			return searchSample(cdf, r, median, end);
		}
	}

	public static void main(String[] args) throws Exception {

		// test sampling from the CDF
		double[] cdf = {0.1, 0.1, 0.1, 0.2, 0.5, 0.7, 0.8, 0.9, 1.0, 1.0};
		System.out.println(searchSample(cdf, 0.1, 0, cdf.length-1));
		System.out.println(searchSample(cdf, 0.33, 0, cdf.length-1));
		System.out.println(searchSample(cdf, 0.5, 0, cdf.length-1));
		System.out.println(searchSample(cdf, 1.0, 0, cdf.length-1));

		// test rounding and pruning to significant digits
		System.out.println(roundToSignificantFigures(17987,3));
		System.out.println(pruneToSignificantFigures(17987, 3));
		System.out.println(roundToSignificantFigures(-17987, 3));
		System.out.println(pruneToSignificantFigures(-17987, 3));
		System.out.println(roundToSignificantFigures(7.9756,3));
		System.out.println(pruneToSignificantFigures(7.9756, 3));
		System.out.println(roundToSignificantFigures(-7.9756,3));
		System.out.println(pruneToSignificantFigures(-7.9756, 3));
		System.out.println(roundToSignificantFigures(0.00000000001234, 5));
		System.out.println(pruneToSignificantFigures(0.00000000001234, 5));

		System.out.println(magnitude(17987,3));
		System.out.println(magnitude(-17987, 3));
	}
}
