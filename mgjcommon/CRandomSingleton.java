/**
 * @author      Marek Grzes	<mgrzes@uwaterloo.ca>
 * @version                        
 * @since       2012-02-14
 */

package mgjcommon;

import java.util.*;
import java.io.*;

public class CRandomSingleton {
	private static CRandomSingleton _instance = null;
	private Random _random;
	public static long _seed = -1;
	public static String _logStem;

	private CRandomSingleton() {
		if ( _seed == -1 ) {
		 _seed = System.currentTimeMillis();
		}
	    _random = new Random(_seed);
	    try{
	    	String tmpdir = System.getProperty("java.io.tmpdir");
			String file = tmpdir+"/lastseed.txt";
			if ( _logStem != null ) {
				file = _logStem + "LastSeed.txt";
			}
	    	FileWriter fstream = new FileWriter(file, true);
	    	BufferedWriter out = new BufferedWriter(fstream);
	    	out.write(_seed+"\n");
	    	out.close();
	    }catch (Exception e){
	    	System.err.println("CRandomSingleton Error: " + e.getMessage());
		    e.printStackTrace();
	    	System.exit(1);
	    }   
	}
	    
	public static CRandomSingleton getInstance() {
		if(_instance == null) {
			_instance = new CRandomSingleton();
	    }
	    return _instance;
	}
	
	public Random getGenertor() {
		return _random;
	}
	
	/**
	 * Test the generator.
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		CRandomSingleton._seed = 1000;
		for (int i=0; i < 10; i++ ) {
			System.out.println(CRandomSingleton.getInstance().getGenertor().nextInt());
		}
	}
}
