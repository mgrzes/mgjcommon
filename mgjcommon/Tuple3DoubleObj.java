package mgjcommon;

public final class Tuple3DoubleObj<C> {
    public double x1;
    public double x2;
    public double x3;
	public C x4;

    public Tuple3DoubleObj() {
    	super();
    }

    public Tuple3DoubleObj(double first, double second, double third, C fourth) {
        super();
        this.x1 = first;
        this.x2 = second;
		this.x3 = third;
        this.x4 = fourth;
    }

    public final int hashCode() {
        return (int)(x1 * 10 + x2 * 100 + x3 * 1000 + x4.hashCode() * 1317);
    }

    public final boolean equals(Object other) {
        if (other instanceof Tuple3DoubleObj) {
        		@SuppressWarnings("unchecked")
                Tuple3DoubleObj otherPair = (Tuple3DoubleObj) other;
                return 
                ( ( this.x1 == otherPair.x1 ) && (  this.x2 == otherPair.x2 ) && ( this.x3 == otherPair.x3 ) && (  this.x4.equals(otherPair.x4) ) );
        }

        return false;
    }

    public String toString()  {
           return "<" + x1 + ", " + x2 + ", " + x3 + ", " + x4.toString() + ">";
    }
}
