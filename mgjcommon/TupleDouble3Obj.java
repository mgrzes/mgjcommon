package mgjcommon;

public final class TupleDouble3Obj<B,C,D> {
    public double x1;
    public B x2;
	public C x3;
	public D x4;

    public TupleDouble3Obj() {
    	super();
    }

    public TupleDouble3Obj(double first, B second, C third, D fourth) {
        super();
        this.x1 = first;
        this.x2 = second;
		this.x3 = third;
		this.x4 = fourth;
    }

    public final int hashCode() {
        return (int)(x1 * 11 + x2.hashCode() * 113 + x3.hashCode() * 1117 + x4.hashCode() * 137913);
    }

    public final boolean equals(Object other) {
        if (other instanceof TupleDouble3Obj) {
        		@SuppressWarnings("unchecked")
				TupleDouble3Obj otherPair = (TupleDouble3Obj) other;
                return 
                ( ( this.x1 == otherPair.x1 ) && (  this.x2.equals(otherPair.x2) ) && (  this.x3.equals(otherPair.x3) ) && ( this.x4.equals(otherPair.x4) ) );
        }

        return false;
    }

    public String toString()  {
           return "<" + x1 + ", " + x2.toString() + ", " + x3.toString() + ", " + x4.toString() + ">";
    }
}
