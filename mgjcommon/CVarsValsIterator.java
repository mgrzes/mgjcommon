/**
 * @author      Marek Grzes	<mgrzes@uwaterloo.ca>
 * @version     1.0           
 * @since       2012-02-10
 */

package mgjcommon;

import java.util.*;

/**
 * This is a variable-value iterator. It is a generic class and can be
 * used in different applications. The only assumption it makes is that
 * variables and values within variables have fixed order.
 */
public class CVarsValsIterator {
	
	/**
	 * For every variable, stores the number of values the variable has.
	 */
	protected int[] _num_vals;
	
	public int[] _current;
	public int[] _result;
	public Set<Integer>	_locked_vars;
	public int	_steps;
	public int	_size;
	public boolean	_has_next;
			
	public CVarsValsIterator(int[] num_vals) {
		_num_vals = num_vals;
		_current = new int[num_vals.length]; // all initial values are 0
		_result = new int[num_vals.length];  // so that we allocate the result only once
		int size = 1;
		for ( int i = 0; i < num_vals.length; i++ ) {
			size *= num_vals[i];
		}
		_steps = 0;
		_size = size;
		_has_next = true;
		_locked_vars = new HashSet<Integer>();
	}

	public void lock_variable(int var_id) {
		_locked_vars.add(new Integer(var_id));
	}
	
	public void unlock_variables() {
		_locked_vars.clear();
	}
	
	public boolean has_next() {
		return _has_next;
	}
	
	public int[] next() throws Exception {
		if ( CRuntimeConfig._DEBUG == true ) { // DEBUG
			if ( ! (_steps++ <= _size) || !has_next() ) {
				throw new Exception("iterator inconsistent");
			}
		} // DEBUG		
		
		// Copy the current state to the result
		System.arraycopy(_current, 0, _result, 0, _current.length);
		
		boolean last_state = true;
		for ( int t = 0; t < _current.length; t++ ) {
			if ( _current[t] < _num_vals[t] - 1 && ( !_locked_vars.contains(t) )  ) {
				last_state = false;
				break;
			}
		}
		if ( last_state ) {
			_has_next = false;
			return _result;
		}
		
		int c = 0;
		boolean added = false;
		for ( int t = 0; t < _num_vals.length; t++ ) {
			if ( _locked_vars.contains(t) ) {
				continue; // simply skip the locked variable
			}
			if ( ! added ) {
				_current[t] = _current[t] + 1;
				added = true;
			}
			_current[t] = _current[t] + c;
			if ( _current[t] == _num_vals[t] ) {
				c = 1;
				_current[t]=0;
			} else {
				c = 0;
			}
		}
		
		return _result;
	}
	
	/**
	 * Resets the iterator only. The set of locked variables is left unchanged.
	 */
	public void reset() {
		for ( int t = 0; t < _current.length; t++ ) {
			_current[t] = 0;
		}
		_steps = 0;
		_has_next = true;	
	}

	/**
	 * This is an example that shows how to use this class.
	 */
	public static void main(String[] args) {
		try {
			
			// This array specifies the number of values the n-th variable can have
			// where n is the index in the array num_vals.
			int[] num_vals = { 2, 2, 3, 5};
			CVarsValsIterator iter = new CVarsValsIterator(num_vals);
			while ( iter.has_next() ) {
				// this reads another enumerated
				int[] next_enumerated_state = iter.next();
				System.out.println("\nNext full enemarated state:");
				int var = 0;
				for ( int val : next_enumerated_state ) {
					System.out.println("Variable number " + var + " has value number " + val );
					var++;
				}
			}
			// if you need to iterate again you need to call: iter.reset();
			
		} catch( Exception exp ) {
		    System.out.println( "Unexpected exception:" + exp.getMessage() );
		    exp.printStackTrace();
		}
	}
}
