/**
 * This is a useful equivalent of stl::pair from C++.
 * This one if for the case when the first element is double.
 */

package mgjcommon;

public final class PairDoubleObj<B> {
    public double first;
    public B second;

    public PairDoubleObj(double first, B second) {
        super();
        this.first = first;
        this.second = second;
    }

    public final int hashCode() {
        int hashSecond = second != null ? second.hashCode() : 0;

        return ( (int)first + hashSecond) * hashSecond + (int)first;
    }

    public final boolean equals(Object other) {
        if (other instanceof PairDoubleObj) {
        		@SuppressWarnings("unchecked")
                PairDoubleObj<B> otherPair = (PairDoubleObj<B>) other;
                return 
                ((  this.first == otherPair.first ) &&
                 (  this.second == otherPair.second ||
                        ( this.second != null && otherPair.second != null &&
                          this.second.equals(otherPair.second))) );
        }

        return false;
    }

    public String toString()
    { 
           return "(" + first + ", " + second + ")"; 
    }
}
