/**
 * @author Marek Grzes <mgrzes@uwaterloo.ca>
 *
 */
package mgjcommon;

import java.util.*;

/**
 * This array can have some large capacity that is not reset when
 * the clear method is invoked.
 *
 */
public final class CArrayCached<T> {
	public T[] _data;
	/**
	 * This is the actual number of elements stored in the array.
	 */
	public int _size;
	protected int _capacity;
	protected int _increment;

	public CArrayCached() {
		this(100, 100);
	}
	
	public CArrayCached(int initialCapacity, int increment) {
		if ( initialCapacity < 0 ) {
			throw new IllegalArgumentException( "Illegal Capacity: " + initialCapacity );
		}
		_data = (T[]) new Object[initialCapacity];
		_size = 0;
		_capacity = initialCapacity;
		_increment = increment;
	}
	
	/**
	 * Not required, _data[] can be accessed directly.
	 */
	public final T get(int i) {
		return _data[i];
	}

	/**
	 * Not required, _data[] can be accessed directly.
	 */
	public final void set(int i, T value) {
		_data[i] = value;
	}
	
	public final void add(T element) {
		_size++;
		if ( _capacity >= _size ) {
			_data[_size - 1] = element;
		} else {
			_capacity += _increment;
			T[] newArray = (T[]) java.lang.reflect.Array.newInstance( element.getClass(), _capacity);
			System.arraycopy(_data,0,newArray,0,_data.length);
			_data = newArray;
			_data[_size - 1] = element;
		}
	}
	
	/**
	 * The container will keep the data, only the size is set to 0.
	 */
	public final void clear() {
		_size = 0;
	}
	
	public final int size() {
		return _size;
	}
}
