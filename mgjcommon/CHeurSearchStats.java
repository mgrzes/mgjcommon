package mgjcommon;

/**
 * Created with IntelliJ IDEA.
 * User: Marek Grzes
 * Date: 20/07/12
 * Time: 10:18 AM
*/

import java.math.BigInteger;

/**
 * For collecting statistics about heuristic search algorithms.
 */
public class CHeurSearchStats {
	/**
	 * Number of times the initial solution was improved, this is useful for DFS algorithms.
	 */
	public long _nImproved;

	/**
	 * Number of times the heuristic was computed.
	 */
	public long _nHeurEvalsOfStates;

	/**
	 * Number of all (fully assigned) states in the search space.
	 */
	public BigInteger _nStates;
}
