/**
 * This is a useful equivalent of stl::pair from C++.
 * This one if for the case when the first element is integer.
 */

package mgjcommon;

public final class PairIntInt {
    public int first;
    public int second;

    public PairIntInt() {
    	super();
    }
    
    public PairIntInt(int first, int second) {
        super();
        this.first = first;
        this.second = second;
    }

    public final int hashCode() {
        int hashSecond = second;
        return (first + hashSecond) * hashSecond + first;
    }

    public final boolean equals(Object other) {
        if (other instanceof PairIntInt) {
        		@SuppressWarnings("unchecked")
                PairIntInt otherPair = (PairIntInt) other;
                return 
                ( ( this.first == otherPair.first ) && (  this.second == otherPair.second) );
        }

        return false;
    }

    public String toString()
    { 
           return "(" + first + ", " + second + ")"; 
    }
}
