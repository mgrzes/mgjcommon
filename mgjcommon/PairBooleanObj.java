/**
 * This is a useful equivalent of stl::pair from C++.
 * This one if for the case when the first element is integer.
 */

package mgjcommon;

public final class PairBooleanObj<B> {
    public boolean first;
    public B second;

    public PairBooleanObj(boolean first, B second) {
        super();
        this.first = first;
        this.second = second;
    }

    public final int hashCode() {
        int hashSecond = second != null ? second.hashCode() : 0;

        return ( ( first ? 1 : 0 ) + hashSecond) * hashSecond + (first ? 1 : 0);
    }

    public final boolean equals(Object other) {
        if (other instanceof PairBooleanObj) {
        		@SuppressWarnings("unchecked")
				PairBooleanObj<B> otherPair = (PairBooleanObj<B>) other;
                return 
                ((  this.first == otherPair.first ) &&
                 (  this.second == otherPair.second ||
                        ( this.second != null && otherPair.second != null &&
                          this.second.equals(otherPair.second))) );
        }

        return false;
    }

    public String toString()
    { 
           return "(" + first + ", " + second + ")"; 
    }
}
